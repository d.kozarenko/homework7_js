"use strict";

let array = [
  "hello",
  ["Boston", "Miami", ["Tatooine"]],
  "Kiev",
  "Kharkiv",
  "Odessa",
  "Lviv",
];

function arrayToList(array, parent = document.body) {
  let string = ``;
  string = array
    .map(function (element) {
      if (typeof element === `object` && typeof element !== null) {
        return `<li><ul>` + arrayToList(element) + `</ul></li>`;
      }
      return `<li>` + element + `</li>`;
    })
    .join(``);

  return string;
}
function pageCleaning() {
  document.body.innerHTML = ``;
}
let list = document.createElement("ul");
list.innerHTML = arrayToList(array);
document.body.append(list);

setTimeout(pageCleaning, 3000);
let timeLeft = document.querySelector(`.time-left`);
let delay = 3;
timeLeft.textContent = delay;
let timerId = setTimeout(function interval() {
  timeLeft.textContent = --delay;
  if (timeLeft.textContent === `0`) {
    return;
  }
  timerId = setTimeout(interval, 1000);
}, 1000);
